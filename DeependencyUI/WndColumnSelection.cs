﻿using System;
using System.Windows.Forms;

namespace Deependency
{
	public partial class WndColumnSelection : Form
	{
		public WndColumnSelection()
		{
			InitializeComponent();
		}

		public void btnSave_Click(object sender, EventArgs e)
		{
			try
			{

				Properties.Settings.Default.ShowVersionColumn = chkVersion.Checked;
				Properties.Settings.Default.ShowCultureColumn = chkCulture.Checked;
				Properties.Settings.Default.ShowProcessorArchitectureColumn = chkProcessorArchitecture.Checked;
				Properties.Settings.Default.ShowPublicKeyTokenColumn = chkPublicKeyToken.Checked;
				Properties.Settings.Default.ShowFileInProgramFolderColumn = chkFileInProgramFolder.Checked;
				Properties.Settings.Default.ShowFileInLocalFolderOrGACColumn = chkFileInLocalFolderOrGAC.Checked;

				Properties.Settings.Default.Save();

				this.DialogResult = DialogResult.OK;
				this.Close();
			}
			catch(Exception ex)
			{
				MessageBox.Show("Error while saving your settings. Please try again later.");
			}
		}

		private void WndColumnSelection_Load(object sender, EventArgs e)
		{
			chkVersion.Checked = Properties.Settings.Default.ShowVersionColumn;
			chkCulture.Checked = Properties.Settings.Default.ShowCultureColumn;
			chkProcessorArchitecture.Checked = Properties.Settings.Default.ShowProcessorArchitectureColumn;
			chkPublicKeyToken.Checked = Properties.Settings.Default.ShowPublicKeyTokenColumn;
			chkFileInProgramFolder.Checked = Properties.Settings.Default.ShowFileInProgramFolderColumn;
			chkFileInLocalFolderOrGAC.Checked = Properties.Settings.Default.ShowFileInLocalFolderOrGACColumn;
		}
	}
}