﻿namespace Deependency
{
	partial class WndMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.txtSelectAssembly = new System.Windows.Forms.TextBox();
			this.btnSelectAssembly = new System.Windows.Forms.Button();
			this.btnAnalyze = new System.Windows.Forms.Button();
			this.dgvDependencies = new System.Windows.Forms.DataGridView();
			this.tcMain = new System.Windows.Forms.TabControl();
			this.tpDependencies = new System.Windows.Forms.TabPage();
			this.tpUnneededAssemblies = new System.Windows.Forms.TabPage();
			this.dgvUnneededAssemblies = new System.Windows.Forms.DataGridView();
			this.cmsDependencies = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.tsmiCopyLFGACToPF = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiManuallySelectAssembly = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
			this.cmsUnneededAssemblies = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.tsmiDeleteAssembly = new System.Windows.Forms.ToolStripMenuItem();
			this.button1 = new System.Windows.Forms.Button();
			this.msMain = new System.Windows.Forms.MenuStrip();
			this.tsmiFile = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiClose = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiSettings = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmiColumnSelection = new System.Windows.Forms.ToolStripMenuItem();
			this.dgvcDependencyAssemblyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvcDependencyAssemblyVersion = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvcDependencyAssemblyProcessorArchitecture = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvcDependencyAssemblyCulture = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvcDependencyAssemblyPublicKeyToken = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvcDependencyFoundInProgramFolder = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.dgvcDependencyFoundInLocalFolderOrGAC = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.dgvcDependencyFileInProgramFolder = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvcDependencyFileInLocalFolderOrGAC = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvcUnneededAssemblyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvcUnneededAssemblyVersion = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvcUnneededAssemblyProcessorArchitecture = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvcUnneededAssemblyCulture = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvcUnneededAssemblyPublicKeyToken = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvcUnneededFoundInProgramFolder = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.dgvcUnneededFileInProgramFolder = new System.Windows.Forms.DataGridViewTextBoxColumn();
			((System.ComponentModel.ISupportInitialize)(this.dgvDependencies)).BeginInit();
			this.tcMain.SuspendLayout();
			this.tpDependencies.SuspendLayout();
			this.tpUnneededAssemblies.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvUnneededAssemblies)).BeginInit();
			this.cmsDependencies.SuspendLayout();
			this.cmsUnneededAssemblies.SuspendLayout();
			this.msMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// txtSelectAssembly
			// 
			this.txtSelectAssembly.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
			| System.Windows.Forms.AnchorStyles.Right)));
			this.txtSelectAssembly.Location = new System.Drawing.Point(12, 29);
			this.txtSelectAssembly.Name = "txtSelectAssembly";
			this.txtSelectAssembly.Size = new System.Drawing.Size(1155, 20);
			this.txtSelectAssembly.TabIndex = 0;
			// 
			// btnSelectAssembly
			// 
			this.btnSelectAssembly.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSelectAssembly.Location = new System.Drawing.Point(1173, 27);
			this.btnSelectAssembly.Name = "btnSelectAssembly";
			this.btnSelectAssembly.Size = new System.Drawing.Size(28, 23);
			this.btnSelectAssembly.TabIndex = 1;
			this.btnSelectAssembly.Text = "...";
			this.btnSelectAssembly.UseVisualStyleBackColor = true;
			this.btnSelectAssembly.Click += new System.EventHandler(this.btnSelectAssembly_Click);
			// 
			// btnAnalyze
			// 
			this.btnAnalyze.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnAnalyze.Location = new System.Drawing.Point(1126, 56);
			this.btnAnalyze.Name = "btnAnalyze";
			this.btnAnalyze.Size = new System.Drawing.Size(75, 23);
			this.btnAnalyze.TabIndex = 2;
			this.btnAnalyze.Text = "Analyze";
			this.btnAnalyze.UseVisualStyleBackColor = true;
			this.btnAnalyze.Click += new System.EventHandler(this.btnAnalyze_Click);
			// 
			// dgvDependencies
			// 
			this.dgvDependencies.AllowUserToAddRows = false;
			this.dgvDependencies.AllowUserToDeleteRows = false;
			this.dgvDependencies.AllowUserToResizeRows = false;
			this.dgvDependencies.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvDependencies.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvcDependencyAssemblyName,
            this.dgvcDependencyAssemblyVersion,
            this.dgvcDependencyAssemblyProcessorArchitecture,
            this.dgvcDependencyAssemblyCulture,
            this.dgvcDependencyAssemblyPublicKeyToken,
            this.dgvcDependencyFoundInProgramFolder,
            this.dgvcDependencyFoundInLocalFolderOrGAC,
            this.dgvcDependencyFileInProgramFolder,
            this.dgvcDependencyFileInLocalFolderOrGAC});
			this.dgvDependencies.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgvDependencies.Location = new System.Drawing.Point(3, 3);
			this.dgvDependencies.MinimumSize = new System.Drawing.Size(4, 4);
			this.dgvDependencies.Name = "dgvDependencies";
			this.dgvDependencies.ReadOnly = true;
			this.dgvDependencies.RowHeadersVisible = false;
			this.dgvDependencies.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvDependencies.Size = new System.Drawing.Size(1175, 549);
			this.dgvDependencies.TabIndex = 3;
			// 
			// tcMain
			// 
			this.tcMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			| System.Windows.Forms.AnchorStyles.Left)
			| System.Windows.Forms.AnchorStyles.Right)));
			this.tcMain.Controls.Add(this.tpDependencies);
			this.tcMain.Controls.Add(this.tpUnneededAssemblies);
			this.tcMain.Location = new System.Drawing.Point(12, 85);
			this.tcMain.Name = "tcMain";
			this.tcMain.SelectedIndex = 0;
			this.tcMain.Size = new System.Drawing.Size(1189, 581);
			this.tcMain.TabIndex = 4;
			// 
			// tpDependencies
			// 
			this.tpDependencies.Controls.Add(this.dgvDependencies);
			this.tpDependencies.Location = new System.Drawing.Point(4, 22);
			this.tpDependencies.Name = "tpDependencies";
			this.tpDependencies.Padding = new System.Windows.Forms.Padding(3);
			this.tpDependencies.Size = new System.Drawing.Size(1181, 555);
			this.tpDependencies.TabIndex = 0;
			this.tpDependencies.Text = "Dependencies";
			this.tpDependencies.UseVisualStyleBackColor = true;
			// 
			// tpUnneededAssemblies
			// 
			this.tpUnneededAssemblies.Controls.Add(this.dgvUnneededAssemblies);
			this.tpUnneededAssemblies.Location = new System.Drawing.Point(4, 22);
			this.tpUnneededAssemblies.Name = "tpUnneededAssemblies";
			this.tpUnneededAssemblies.Padding = new System.Windows.Forms.Padding(3);
			this.tpUnneededAssemblies.Size = new System.Drawing.Size(1181, 555);
			this.tpUnneededAssemblies.TabIndex = 1;
			this.tpUnneededAssemblies.Text = "Unneeded Assemblies";
			this.tpUnneededAssemblies.UseVisualStyleBackColor = true;
			// 
			// dgvUnneededAssemblies
			// 
			this.dgvUnneededAssemblies.AllowUserToAddRows = false;
			this.dgvUnneededAssemblies.AllowUserToDeleteRows = false;
			this.dgvUnneededAssemblies.AllowUserToResizeRows = false;
			this.dgvUnneededAssemblies.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvUnneededAssemblies.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvcUnneededAssemblyName,
            this.dgvcUnneededAssemblyVersion,
            this.dgvcUnneededAssemblyProcessorArchitecture,
            this.dgvcUnneededAssemblyCulture,
            this.dgvcUnneededAssemblyPublicKeyToken,
            this.dgvcUnneededFoundInProgramFolder,
            this.dgvcUnneededFileInProgramFolder});
			this.dgvUnneededAssemblies.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgvUnneededAssemblies.Location = new System.Drawing.Point(3, 3);
			this.dgvUnneededAssemblies.Name = "dgvUnneededAssemblies";
			this.dgvUnneededAssemblies.ReadOnly = true;
			this.dgvUnneededAssemblies.RowHeadersVisible = false;
			this.dgvUnneededAssemblies.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvUnneededAssemblies.Size = new System.Drawing.Size(1175, 549);
			this.dgvUnneededAssemblies.TabIndex = 4;
			// 
			// cmsDependencies
			// 
			this.cmsDependencies.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCopyLFGACToPF,
            this.tsmiManuallySelectAssembly,
            this.toolStripMenuItem2});
			this.cmsDependencies.Name = "cmsDependencies";
			this.cmsDependencies.Size = new System.Drawing.Size(199, 70);
			// 
			// tsmiCopyLFGACToPF
			// 
			this.tsmiCopyLFGACToPF.Name = "tsmiCopyLFGACToPF";
			this.tsmiCopyLFGACToPF.Size = new System.Drawing.Size(198, 22);
			this.tsmiCopyLFGACToPF.Text = "Copy LF/GAC to PF";
			// 
			// tsmiManuallySelectAssembly
			// 
			this.tsmiManuallySelectAssembly.Name = "tsmiManuallySelectAssembly";
			this.tsmiManuallySelectAssembly.Size = new System.Drawing.Size(198, 22);
			this.tsmiManuallySelectAssembly.Text = "Manually add assembly";
			// 
			// toolStripMenuItem2
			// 
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			this.toolStripMenuItem2.Size = new System.Drawing.Size(198, 22);
			this.toolStripMenuItem2.Text = "Options";
			// 
			// cmsUnneededAssemblies
			// 
			this.cmsUnneededAssemblies.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiDeleteAssembly});
			this.cmsUnneededAssemblies.Name = "cmsUnneededAssemblies";
			this.cmsUnneededAssemblies.Size = new System.Drawing.Size(160, 26);
			// 
			// tsmiDeleteAssembly
			// 
			this.tsmiDeleteAssembly.Name = "tsmiDeleteAssembly";
			this.tsmiDeleteAssembly.Size = new System.Drawing.Size(159, 22);
			this.tsmiDeleteAssembly.Text = "Delete assembly";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(0, 0);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 19;
			// 
			// msMain
			// 
			this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFile,
            this.tsmiSettings});
			this.msMain.Location = new System.Drawing.Point(0, 0);
			this.msMain.Name = "msMain";
			this.msMain.Size = new System.Drawing.Size(1213, 24);
			this.msMain.TabIndex = 18;
			this.msMain.Text = "menuStrip1";
			// 
			// tsmiFile
			// 
			this.tsmiFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiClose});
			this.tsmiFile.Name = "tsmiFile";
			this.tsmiFile.Size = new System.Drawing.Size(37, 20);
			this.tsmiFile.Text = "File";
			// 
			// tsmiClose
			// 
			this.tsmiClose.Name = "tsmiClose";
			this.tsmiClose.Size = new System.Drawing.Size(103, 22);
			this.tsmiClose.Text = "Close";
			this.tsmiClose.Click += new System.EventHandler(this.tsmiClose_Click);
			// 
			// tsmiSettings
			// 
			this.tsmiSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiColumnSelection});
			this.tsmiSettings.Name = "tsmiSettings";
			this.tsmiSettings.Size = new System.Drawing.Size(61, 20);
			this.tsmiSettings.Text = "Settings";
			// 
			// tsmiColumnSelection
			// 
			this.tsmiColumnSelection.Name = "tsmiColumnSelection";
			this.tsmiColumnSelection.Size = new System.Drawing.Size(168, 22);
			this.tsmiColumnSelection.Text = "Column Selection";
			this.tsmiColumnSelection.Click += new System.EventHandler(this.tsmiColumnSelection_Click);
			// 
			// dgvcDependencyAssemblyName
			// 
			this.dgvcDependencyAssemblyName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.dgvcDependencyAssemblyName.FillWeight = 112.2302F;
			this.dgvcDependencyAssemblyName.HeaderText = "Assembly";
			this.dgvcDependencyAssemblyName.MinimumWidth = 40;
			this.dgvcDependencyAssemblyName.Name = "dgvcDependencyAssemblyName";
			this.dgvcDependencyAssemblyName.ReadOnly = true;
			// 
			// dgvcDependencyAssemblyVersion
			// 
			this.dgvcDependencyAssemblyVersion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.dgvcDependencyAssemblyVersion.FillWeight = 112.2302F;
			this.dgvcDependencyAssemblyVersion.HeaderText = "Version";
			this.dgvcDependencyAssemblyVersion.MinimumWidth = 40;
			this.dgvcDependencyAssemblyVersion.Name = "dgvcDependencyAssemblyVersion";
			this.dgvcDependencyAssemblyVersion.ReadOnly = true;
			// 
			// dgvcDependencyAssemblyProcessorArchitecture
			// 
			this.dgvcDependencyAssemblyProcessorArchitecture.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.dgvcDependencyAssemblyProcessorArchitecture.FillWeight = 112.2302F;
			this.dgvcDependencyAssemblyProcessorArchitecture.HeaderText = "x86/x64";
			this.dgvcDependencyAssemblyProcessorArchitecture.MinimumWidth = 40;
			this.dgvcDependencyAssemblyProcessorArchitecture.Name = "dgvcDependencyAssemblyProcessorArchitecture";
			this.dgvcDependencyAssemblyProcessorArchitecture.ReadOnly = true;
			// 
			// dgvcDependencyAssemblyCulture
			// 
			this.dgvcDependencyAssemblyCulture.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.dgvcDependencyAssemblyCulture.HeaderText = "Culture";
			this.dgvcDependencyAssemblyCulture.MinimumWidth = 40;
			this.dgvcDependencyAssemblyCulture.Name = "dgvcDependencyAssemblyCulture";
			this.dgvcDependencyAssemblyCulture.ReadOnly = true;
			this.dgvcDependencyAssemblyCulture.Visible = false;
			// 
			// dgvcDependencyAssemblyPublicKeyToken
			// 
			this.dgvcDependencyAssemblyPublicKeyToken.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.dgvcDependencyAssemblyPublicKeyToken.HeaderText = "PublicKeyToken";
			this.dgvcDependencyAssemblyPublicKeyToken.MinimumWidth = 40;
			this.dgvcDependencyAssemblyPublicKeyToken.Name = "dgvcDependencyAssemblyPublicKeyToken";
			this.dgvcDependencyAssemblyPublicKeyToken.ReadOnly = true;
			this.dgvcDependencyAssemblyPublicKeyToken.Visible = false;
			// 
			// dgvcDependencyFoundInProgramFolder
			// 
			this.dgvcDependencyFoundInProgramFolder.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.dgvcDependencyFoundInProgramFolder.FillWeight = 38.84892F;
			this.dgvcDependencyFoundInProgramFolder.HeaderText = "PF?";
			this.dgvcDependencyFoundInProgramFolder.MinimumWidth = 40;
			this.dgvcDependencyFoundInProgramFolder.Name = "dgvcDependencyFoundInProgramFolder";
			this.dgvcDependencyFoundInProgramFolder.ReadOnly = true;
			this.dgvcDependencyFoundInProgramFolder.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.dgvcDependencyFoundInProgramFolder.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			this.dgvcDependencyFoundInProgramFolder.Width = 51;
			// 
			// dgvcDependencyFoundInLocalFolderOrGAC
			// 
			this.dgvcDependencyFoundInLocalFolderOrGAC.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.dgvcDependencyFoundInLocalFolderOrGAC.HeaderText = "LF/GAC?";
			this.dgvcDependencyFoundInLocalFolderOrGAC.MinimumWidth = 40;
			this.dgvcDependencyFoundInLocalFolderOrGAC.Name = "dgvcDependencyFoundInLocalFolderOrGAC";
			this.dgvcDependencyFoundInLocalFolderOrGAC.ReadOnly = true;
			this.dgvcDependencyFoundInLocalFolderOrGAC.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.dgvcDependencyFoundInLocalFolderOrGAC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			this.dgvcDependencyFoundInLocalFolderOrGAC.Width = 77;
			// 
			// dgvcDependencyFileInProgramFolder
			// 
			this.dgvcDependencyFileInProgramFolder.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.dgvcDependencyFileInProgramFolder.FillWeight = 112.2302F;
			this.dgvcDependencyFileInProgramFolder.HeaderText = "Location in program folder";
			this.dgvcDependencyFileInProgramFolder.MinimumWidth = 40;
			this.dgvcDependencyFileInProgramFolder.Name = "dgvcDependencyFileInProgramFolder";
			this.dgvcDependencyFileInProgramFolder.ReadOnly = true;
			// 
			// dgvcDependencyFileInLocalFolderOrGAC
			// 
			this.dgvcDependencyFileInLocalFolderOrGAC.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.dgvcDependencyFileInLocalFolderOrGAC.FillWeight = 112.2302F;
			this.dgvcDependencyFileInLocalFolderOrGAC.HeaderText = "Location in local folder or GAC";
			this.dgvcDependencyFileInLocalFolderOrGAC.MinimumWidth = 40;
			this.dgvcDependencyFileInLocalFolderOrGAC.Name = "dgvcDependencyFileInLocalFolderOrGAC";
			this.dgvcDependencyFileInLocalFolderOrGAC.ReadOnly = true;
			// 
			// dgvcUnneededAssemblyName
			// 
			this.dgvcUnneededAssemblyName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.dgvcUnneededAssemblyName.FillWeight = 129.2783F;
			this.dgvcUnneededAssemblyName.HeaderText = "Assembly";
			this.dgvcUnneededAssemblyName.MinimumWidth = 40;
			this.dgvcUnneededAssemblyName.Name = "dgvcUnneededAssemblyName";
			this.dgvcUnneededAssemblyName.ReadOnly = true;
			// 
			// dgvcUnneededAssemblyVersion
			// 
			this.dgvcUnneededAssemblyVersion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.dgvcUnneededAssemblyVersion.FillWeight = 142.132F;
			this.dgvcUnneededAssemblyVersion.HeaderText = "Version";
			this.dgvcUnneededAssemblyVersion.MinimumWidth = 70;
			this.dgvcUnneededAssemblyVersion.Name = "dgvcUnneededAssemblyVersion";
			this.dgvcUnneededAssemblyVersion.ReadOnly = true;
			// 
			// dgvcUnneededAssemblyProcessorArchitecture
			// 
			this.dgvcUnneededAssemblyProcessorArchitecture.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.dgvcUnneededAssemblyProcessorArchitecture.HeaderText = "x86/x64";
			this.dgvcUnneededAssemblyProcessorArchitecture.MinimumWidth = 40;
			this.dgvcUnneededAssemblyProcessorArchitecture.Name = "dgvcUnneededAssemblyProcessorArchitecture";
			this.dgvcUnneededAssemblyProcessorArchitecture.ReadOnly = true;
			// 
			// dgvcUnneededAssemblyCulture
			// 
			this.dgvcUnneededAssemblyCulture.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.dgvcUnneededAssemblyCulture.HeaderText = "Culture";
			this.dgvcUnneededAssemblyCulture.MinimumWidth = 40;
			this.dgvcUnneededAssemblyCulture.Name = "dgvcUnneededAssemblyCulture";
			this.dgvcUnneededAssemblyCulture.ReadOnly = true;
			this.dgvcUnneededAssemblyCulture.Visible = false;
			// 
			// dgvcUnneededAssemblyPublicKeyToken
			// 
			this.dgvcUnneededAssemblyPublicKeyToken.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.dgvcUnneededAssemblyPublicKeyToken.HeaderText = "PublicKeyToken";
			this.dgvcUnneededAssemblyPublicKeyToken.MinimumWidth = 40;
			this.dgvcUnneededAssemblyPublicKeyToken.Name = "dgvcUnneededAssemblyPublicKeyToken";
			this.dgvcUnneededAssemblyPublicKeyToken.ReadOnly = true;
			this.dgvcUnneededAssemblyPublicKeyToken.Visible = false;
			// 
			// dgvcUnneededFoundInProgramFolder
			// 
			this.dgvcUnneededFoundInProgramFolder.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.dgvcUnneededFoundInProgramFolder.HeaderText = "PF?";
			this.dgvcUnneededFoundInProgramFolder.MinimumWidth = 40;
			this.dgvcUnneededFoundInProgramFolder.Name = "dgvcUnneededFoundInProgramFolder";
			this.dgvcUnneededFoundInProgramFolder.ReadOnly = true;
			this.dgvcUnneededFoundInProgramFolder.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.dgvcUnneededFoundInProgramFolder.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			this.dgvcUnneededFoundInProgramFolder.Width = 51;
			// 
			// dgvcUnneededFileInProgramFolder
			// 
			this.dgvcUnneededFileInProgramFolder.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.dgvcUnneededFileInProgramFolder.FillWeight = 72.883F;
			this.dgvcUnneededFileInProgramFolder.HeaderText = "Location in program folder";
			this.dgvcUnneededFileInProgramFolder.MinimumWidth = 40;
			this.dgvcUnneededFileInProgramFolder.Name = "dgvcUnneededFileInProgramFolder";
			this.dgvcUnneededFileInProgramFolder.ReadOnly = true;
			// 
			// WndMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1213, 678);
			this.Controls.Add(this.msMain);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.tcMain);
			this.Controls.Add(this.btnAnalyze);
			this.Controls.Add(this.btnSelectAssembly);
			this.Controls.Add(this.txtSelectAssembly);
			this.MainMenuStrip = this.msMain;
			this.Name = "WndMain";
			this.Text = "Deependency.NET";
			this.Load += new System.EventHandler(this.WndMain_Load);
			((System.ComponentModel.ISupportInitialize)(this.dgvDependencies)).EndInit();
			this.tcMain.ResumeLayout(false);
			this.tpDependencies.ResumeLayout(false);
			this.tpUnneededAssemblies.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvUnneededAssemblies)).EndInit();
			this.cmsDependencies.ResumeLayout(false);
			this.cmsUnneededAssemblies.ResumeLayout(false);
			this.msMain.ResumeLayout(false);
			this.msMain.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtSelectAssembly;
		private System.Windows.Forms.Button btnSelectAssembly;
		private System.Windows.Forms.Button btnAnalyze;
		private System.Windows.Forms.DataGridView dgvDependencies;
		private System.Windows.Forms.TabControl tcMain;
		private System.Windows.Forms.TabPage tpDependencies;
		private System.Windows.Forms.TabPage tpUnneededAssemblies;
		private System.Windows.Forms.DataGridView dgvUnneededAssemblies;
		private System.Windows.Forms.ContextMenuStrip cmsDependencies;
		private System.Windows.Forms.ToolStripMenuItem tsmiCopyLFGACToPF;
		private System.Windows.Forms.ToolStripMenuItem tsmiManuallySelectAssembly;
		private System.Windows.Forms.ContextMenuStrip cmsUnneededAssemblies;
		private System.Windows.Forms.ToolStripMenuItem tsmiDeleteAssembly;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.MenuStrip msMain;
		private System.Windows.Forms.ToolStripMenuItem tsmiFile;
		private System.Windows.Forms.ToolStripMenuItem tsmiClose;
		private System.Windows.Forms.ToolStripMenuItem tsmiSettings;
		private System.Windows.Forms.ToolStripMenuItem tsmiColumnSelection;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvcDependencyAssemblyName;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvcDependencyAssemblyVersion;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvcDependencyAssemblyProcessorArchitecture;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvcDependencyAssemblyCulture;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvcDependencyAssemblyPublicKeyToken;
		private System.Windows.Forms.DataGridViewCheckBoxColumn dgvcDependencyFoundInProgramFolder;
		private System.Windows.Forms.DataGridViewCheckBoxColumn dgvcDependencyFoundInLocalFolderOrGAC;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvcDependencyFileInProgramFolder;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvcDependencyFileInLocalFolderOrGAC;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvcUnneededAssemblyName;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvcUnneededAssemblyVersion;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvcUnneededAssemblyProcessorArchitecture;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvcUnneededAssemblyCulture;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvcUnneededAssemblyPublicKeyToken;
		private System.Windows.Forms.DataGridViewCheckBoxColumn dgvcUnneededFoundInProgramFolder;
		private System.Windows.Forms.DataGridViewTextBoxColumn dgvcUnneededFileInProgramFolder;

	}
}

