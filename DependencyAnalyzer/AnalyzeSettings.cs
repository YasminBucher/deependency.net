﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyAnalyzer
{
	public class AnalyzeSettings
	{
		public string AssemblyFilePath { get; set; }
		public string DependenciesFilePath { get; set; }
		public string UnneededAssembliesFilePath { get; set; }
		public string AssemblyCacheFolderPath { get; set; }

		public AnalyzeSettings(string assemblyFilePath, string dependenciesFilePath, string unneededAssembliesFilePath, string assemblyCacheFolderPath)
		{
			this.AssemblyFilePath = assemblyFilePath;
			this.DependenciesFilePath = dependenciesFilePath;
			this.UnneededAssembliesFilePath = unneededAssembliesFilePath;
			this.AssemblyCacheFolderPath = assemblyCacheFolderPath;
		}
	}
}