# README #

### What is this repository for? ###

Deependency.NET is a simple tool to gather information about direct and deep dependencies of .NET applications.  
It shows you what dependencies are missing in the folder of the application and if they are available in the GAC.

### How do I get set up? ###

We are using Visual Studio 2013 Ultimate with GhostDoc and ReSharper with all rules active. This is the minimum set of rules that the code has to comply to.

### Contribution guidelines ###

You are of course welcome to contribute.  
Just fork the repository, change what you think needs a change and create a pull request. Yes, it is as simple as that!  
Tests are nice to have, but as you can clearly see, we don't do them really ourselfes, so feel free to omit them.

### Who do I talk to? ###

If you have any questions about the project please feel free to contact me (Derpy Hooves / KarillEndusa) in any way possible to you.  
You can find me most of the time (CET) on http://forum.mds-tv.de

### Credits / Contributors ###

KarillEndusa - Main Developer  
Danijel Klaic - Column-Display-Selection