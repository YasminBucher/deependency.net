﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace dcutils
{
	public class AssemblyData : IEquatable<AssemblyData>, IEquatable<Assembly>
	{
		private const string ASSEMBLYDATAFORMAT = "{0};{1};{2};{3};{4}";

		private string _name;
		private Version _version;
		private ProcessorArchitecture _processorArchitecture;
		private string _cultureName;
		private byte[] _publicTokenKey;
		private bool _foundInProgramFolder;
		private string _fileNameInProgramFolder;
		private bool _foundInLocalFolderOrGAC;
		private string _fileNameInLocalFolderOrGAC;

		public string Name { get { return this._name; } }
		public Version Version { get { return this._version; } }
		public ProcessorArchitecture ProcessorArchitecture { get { return this._processorArchitecture; } }
		public string CultureName { get { return this._cultureName; } }
		public byte[] PublicTokenKey { get { return this._publicTokenKey; } }
		public bool FoundInProgramFolder { get { return this._foundInProgramFolder; } }
		public string FileNameInProgramFolder { get { return this._fileNameInProgramFolder; } }
		public bool FoundInLocalFolderOrGAC { get { return this._foundInLocalFolderOrGAC; } }
		public string FileNameInLocalFolderOrGAC { get { return this._fileNameInLocalFolderOrGAC; } }

		public AssemblyData(
			string name,
			Version version,
			ProcessorArchitecture processorArchitecture,
			string cultureName,
			byte[] publicTokenKey,
			bool foundInProgramFolder,
			string fileNameInProgramFolder,
			bool foundInLocalFolderOrGAC,
			string fileNameInLocalFolderOrGAC)
		{
			this._name = name;
			this._version = version;
			this._processorArchitecture = processorArchitecture;
			this._cultureName = cultureName;
			this._publicTokenKey = publicTokenKey;
			this._foundInProgramFolder = foundInProgramFolder;
			this._fileNameInProgramFolder = fileNameInProgramFolder;
			this._foundInLocalFolderOrGAC = foundInLocalFolderOrGAC;
			this._fileNameInLocalFolderOrGAC = fileNameInLocalFolderOrGAC;
		}

		public bool Equals(AssemblyData other)
		{
			return this.GetComparisonString().Equals(other.GetComparisonString());
		}

		public bool Equals(Assembly assembly)
		{
			AssemblyName asmName = assembly.GetName();
			string assemblyString = string.Format(ASSEMBLYDATAFORMAT, asmName.Name, asmName.Version, asmName.ProcessorArchitecture, asmName.CultureName, Utils.ByteArrayToHexString(asmName.GetPublicKeyToken()));

			return this.GetComparisonString().Equals(assemblyString);
		}

		public string GetComparisonString()
		{
			string comparisonString = string.Format(ASSEMBLYDATAFORMAT, this._name, this._version, this._processorArchitecture, this._cultureName, Utils.ByteArrayToHexString(this._publicTokenKey));
			return comparisonString;
		}

		public override string ToString()
		{
			string toString = string.Format("{0};{1};{2};{3};{4}", this.GetComparisonString(), this._foundInProgramFolder, this._fileNameInProgramFolder, this._foundInLocalFolderOrGAC, this._fileNameInLocalFolderOrGAC);
			return toString;
		}

		public static AssemblyData Parse(string s)
		{
			string[] parts = s.Split(';');

			AssemblyData dependency = new dcutils.AssemblyData(
				parts[0],
				Version.Parse(parts[1]),
				(ProcessorArchitecture)Enum.Parse(typeof(ProcessorArchitecture), parts[2]),
				parts[3],
				dcutils.Utils.HexStringToByteArray(parts[4]),
				bool.Parse(parts[5]),
				parts[6],
				bool.Parse(parts[7]),
				parts[8]);

			return dependency;
		}
	}
}