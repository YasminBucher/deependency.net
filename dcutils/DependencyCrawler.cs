﻿using System.Reflection;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

namespace dcutils
{
	public class DependencyCrawler
	{
		public static string[] _assemblyFileExtensions = { ".dll", ".exe" };

		private string _assemblyFolderPath = null;
		private string _baseAssemblyName = null;
		private string _assemblyCacheFolder = null;

		public DependencyCrawler(string assemblyPath, string assemblyCacheFolder)
		{
			this._baseAssemblyName = (new FileInfo(assemblyPath)).Name;
			this._assemblyFolderPath = Path.GetDirectoryName(assemblyPath);
			this._assemblyCacheFolder = assemblyCacheFolder;
		}

		public List<AssemblyData> GetAllDependencies()
		{
			List<AssemblyData> dependencies = new List<AssemblyData>();

			try
			{
				Assembly baseAssembly = Assembly.LoadFrom(Path.Combine(this._assemblyCacheFolder, this._baseAssemblyName));
				dependencies.AddRange(this.GetDependencies(baseAssembly));
			}
			catch
			{
				return null;
			}

			return dependencies;
		}

		private List<AssemblyData> GetDependencies(Assembly baseAssembly)
		{
			List<AssemblyData> dependencies = new List<AssemblyData>();
			this.GatherDependencies(baseAssembly, dependencies);
			return dependencies;
		}

		private void GatherDependencies(Assembly baseAssembly, List<AssemblyData> dependencies)
		{
			AssemblyName[] referencedAssemblies = baseAssembly.GetReferencedAssemblies();
			foreach(AssemblyName assemblyName in referencedAssemblies)
			{
				string assemblyInProgramFolderLocation = null;
				Assembly assemblyInProgramFolder = this.GetAssemblyFromProgramFolder(assemblyName, out assemblyInProgramFolderLocation);
				Assembly assemblyInLocalFolderOrGAC = this.GetAssembly(assemblyName);
				AssemblyData dependency = new AssemblyData(
					assemblyName.Name,
					assemblyName.Version,
					assemblyName.ProcessorArchitecture,
					assemblyName.CultureName,
					assemblyName.GetPublicKeyToken(),
					assemblyInProgramFolder != null,
					assemblyInProgramFolderLocation,
					(assemblyInLocalFolderOrGAC != null) && assemblyInLocalFolderOrGAC.GlobalAssemblyCache,
					(assemblyInLocalFolderOrGAC != null) ? assemblyInLocalFolderOrGAC.Location : null);

				if(!dependencies.Contains(dependency))
				{
					dependencies.Add(dependency);

					if(assemblyInProgramFolder != null)
					{
						this.GatherDependencies(assemblyInProgramFolder, dependencies);
					}
				}
			}
		}

		private Assembly GetAssemblyFromProgramFolder(AssemblyName assemblyName, out string filePath)
		{
			Assembly assembly = null;
			filePath = null;

			foreach(string extension in _assemblyFileExtensions)
			{
				try
				{
					filePath = Path.Combine(this._assemblyFolderPath, assemblyName.Name + extension);
					assembly = Assembly.LoadFrom(filePath);
				}
				catch
				{
					assembly = null;
					filePath = null;
				}

				if(assembly != null)
				{
					break;
				}
			}

			return assembly;
		}

		private Assembly GetAssembly(AssemblyName assemblyName)
		{
			Assembly assembly = null;

			try
			{
				assembly = Assembly.Load(assemblyName);
			}
			catch
			{
				assembly = null;
			}

			return assembly;
		}

		public List<AssemblyData> GetAllUnneededAssemblies(AssemblyData[] dependencies)
		{
			Dictionary<string, Assembly> assemblies = this.GetAssemblies();
			List<AssemblyData> unneededAssemblies = new List<AssemblyData>();

			foreach(KeyValuePair<string, Assembly> assembly in assemblies)
			{
				if(!dependencies.Any(d => d.FileNameInProgramFolder == assembly.Key))
				{
					AssemblyName an = assembly.Value.GetName();
					AssemblyData ad = new AssemblyData(
						an.Name,
						an.Version,
						an.ProcessorArchitecture,
						an.CultureName,
						an.GetPublicKeyToken(),
						true,
						assembly.Key,
						false,
						string.Empty);

					unneededAssemblies.Add(ad);
				}
			}

			return unneededAssemblies;
		}

		private Dictionary<string, Assembly> GetAssemblies()
		{
			Dictionary<string, Assembly> assemblies = new Dictionary<string, Assembly>();

			if(this._assemblyFolderPath != null)
			{
				string[] allFiles = Directory.GetFiles(this._assemblyFolderPath);

				Assembly tmpAssembly = null;
				foreach(string file in allFiles)
				{
					tmpAssembly = null;

					try
					{
						tmpAssembly = Assembly.LoadFrom(file);
					}
					catch
					{
						tmpAssembly = null;
					}

					if(tmpAssembly != null)
					{
						assemblies.Add(file, tmpAssembly);
					}
				}
			}
			else
			{
				return null;
			}

			return assemblies;
		}
	}
}