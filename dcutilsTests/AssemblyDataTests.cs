﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace dcutilsTests
{
	[TestFixture()]
	public class AssemblyDataTests
	{
		[Test()]
		public void ToStringTest()
		{
			dcutils.AssemblyData ad = new dcutils.AssemblyData("Test", new Version(1, 0, 0, 0), System.Reflection.ProcessorArchitecture.X86, "Invariant", new byte[] { 0x01, 0x07, 0xAA }, true, "Hase", false, "");
			string actual = ad.GetComparisonString();
			string expected = "Test;1.0.0.0;X86;Invariant;01 07 AA";

			Assert.AreEqual(expected, actual);
		}
	}
}